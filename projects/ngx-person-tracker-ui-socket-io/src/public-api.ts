/*
 * Public API Surface of ngx-person-tracker-ui-socket-io
 */

export * from './lib/services/socket-io-config.service';
export * from './lib/services/socket-io.service';

export * from './lib/ngx-person-tracker-ui-socket-io.module';
