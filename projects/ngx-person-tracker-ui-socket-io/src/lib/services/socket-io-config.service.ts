import { Injectable } from '@angular/core';

import { ManagerOptions } from 'socket.io-client';

import { commonsSocketIoBuildUrl, commonsSocketIoBuildOptions } from 'ngx-httpcommons-es-socket-io';

import { ConfigService } from 'ngx-person-tracker-ui-shared';

@Injectable({
		providedIn: 'root'
})
export class SocketIoConfigService {
	constructor(
			private configService: ConfigService
	) {}

	public getSocketIoUrl(): string {
		return commonsSocketIoBuildUrl(this.configService.getUrl());
	}
	
	public getSocketIoOptions(): Partial<ManagerOptions> {
		return commonsSocketIoBuildOptions(this.configService.getUrl());
	}
}
