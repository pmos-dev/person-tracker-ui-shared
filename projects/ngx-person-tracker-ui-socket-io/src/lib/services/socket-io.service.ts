import { EventEmitter, Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { commonsBase62HasPropertyId, commonsTypeHasPropertyStringOrUndefined } from 'tscommons-es-core';

import { CommonsStatusService } from 'ngx-angularcommons-es-core';

import { CommonsSocketIoClientService } from 'ngx-httpcommons-es-socket-io';

import {
		TSocketIoInteraction,
		isTSocketIoInteraction,
		TSocketIoPerson,
		isTSocketIoPerson,
		TSocketIoPersonInteraction,
		isTSocketIoPersonInteraction,
		TSocketIoNewData,
		TSocketIoChangeData,
		isTSocketIoNewData,
		isTSocketIoChangeData,
		TSocketIoNewNote,
		TSocketIoChangeNote,
		isTSocketIoNewNote,
		isTSocketIoChangeNote,
		TSocketIoInteractionStep,
		isTSocketIoInteractionStep
} from 'person-tracker-socket-io-ts-assets';

import { SocketIoConfigService } from './socket-io-config.service';

export type TGeneratorId = {
		generatorId?: string;
};
function isTGeneratorId(test: unknown): test is TGeneratorId {
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'generatorId')) return false;

	return true;
}

@Injectable({
		providedIn: 'root'
})
export class SocketIoService extends CommonsSocketIoClientService {
	private onNewInteraction: EventEmitter<TSocketIoInteraction & TGeneratorId> = new EventEmitter<TSocketIoInteraction & TGeneratorId>(true);
	public get newInteractionObservable(): Observable<TSocketIoInteraction & TGeneratorId> {
		return this.onNewInteraction;
	}

	private onDeleteInteraction: EventEmitter<TSocketIoInteraction & TGeneratorId> = new EventEmitter<TSocketIoInteraction & TGeneratorId>(true);
	public get deleteInteractionObservable(): Observable<TSocketIoInteraction & TGeneratorId> {
		return this.onDeleteInteraction;
	}

	private onNewPerson: EventEmitter<TSocketIoPerson & TGeneratorId> = new EventEmitter<TSocketIoPerson & TGeneratorId>(true);
	public get newPersonObservable(): Observable<TSocketIoPerson & TGeneratorId> {
		return this.onNewPerson;
	}

	private onDeletePerson: EventEmitter<TSocketIoPerson & TGeneratorId> = new EventEmitter<TSocketIoPerson & TGeneratorId>(true);
	public get deletePersonObservable(): Observable<TSocketIoPerson & TGeneratorId> {
		return this.onDeletePerson;
	}

	private onLinkPersonInteraction: EventEmitter<TSocketIoPersonInteraction & TGeneratorId> = new EventEmitter<TSocketIoPersonInteraction & TGeneratorId>(true);
	public get linkPersonInteractionObservable(): Observable<TSocketIoPersonInteraction & TGeneratorId> {
		return this.onLinkPersonInteraction;
	}

	private onUnlinkPersonInteraction: EventEmitter<TSocketIoPersonInteraction & TGeneratorId> = new EventEmitter<TSocketIoPersonInteraction & TGeneratorId>(true);
	public get unlinkPersonInteractionObservable(): Observable<TSocketIoPersonInteraction & TGeneratorId> {
		return this.onUnlinkPersonInteraction;
	}

	private onNewInteractionStep: EventEmitter<TSocketIoInteractionStep & TGeneratorId> = new EventEmitter<TSocketIoInteractionStep & TGeneratorId>(true);
	public get newInteractionStepObservable(): Observable<TSocketIoInteractionStep & TGeneratorId> {
		return this.onNewInteractionStep;
	}

	private onNewInteractionData: EventEmitter<TSocketIoNewData & TSocketIoInteraction & TGeneratorId> = new EventEmitter<TSocketIoNewData & TSocketIoInteraction & TGeneratorId>(true);
	public get newInteractionDataObservable(): Observable<TSocketIoNewData & TSocketIoInteraction & TGeneratorId> {
		return this.onNewInteractionData;
	}

	private onNewPersonData: EventEmitter<TSocketIoNewData & TSocketIoPerson & TGeneratorId> = new EventEmitter<TSocketIoNewData & TSocketIoPerson & TGeneratorId>(true);
	public get newPersonDataObservable(): Observable<TSocketIoNewData & TSocketIoPerson & TGeneratorId> {
		return this.onNewPersonData;
	}

	private onUpdateInteractionData: EventEmitter<TSocketIoChangeData & TSocketIoInteraction & TGeneratorId> = new EventEmitter<TSocketIoChangeData & TSocketIoInteraction & TGeneratorId>(true);
	public get updateInteractionDataObservable(): Observable<TSocketIoChangeData & TSocketIoInteraction & TGeneratorId> {
		return this.onUpdateInteractionData;
	}

	private onUpdatePersonData: EventEmitter<TSocketIoChangeData & TSocketIoPerson & TGeneratorId> = new EventEmitter<TSocketIoChangeData & TSocketIoPerson & TGeneratorId>(true);
	public get updatePersonDataObservable(): Observable<TSocketIoChangeData & TSocketIoPerson & TGeneratorId> {
		return this.onUpdatePersonData;
	}

	private onDeleteInteractionData: EventEmitter<TSocketIoChangeData & TSocketIoInteraction & TGeneratorId> = new EventEmitter<TSocketIoChangeData & TSocketIoInteraction & TGeneratorId>(true);
	public get deleteInteractionDataObservable(): Observable<TSocketIoChangeData & TSocketIoInteraction & TGeneratorId> {
		return this.onDeleteInteractionData;
	}

	private onDeletePersonData: EventEmitter<TSocketIoChangeData & TSocketIoPerson & TGeneratorId> = new EventEmitter<TSocketIoChangeData & TSocketIoPerson & TGeneratorId>(true);
	public get deletePersonDataObservable(): Observable<TSocketIoChangeData & TSocketIoPerson & TGeneratorId> {
		return this.onDeletePersonData;
	}

	private onNewInteractionNote: EventEmitter<TSocketIoNewNote & TSocketIoInteraction & TGeneratorId> = new EventEmitter<TSocketIoNewNote & TSocketIoInteraction & TGeneratorId>(true);
	public get newInteractionNoteObservable(): Observable<TSocketIoNewNote & TSocketIoInteraction & TGeneratorId> {
		return this.onNewInteractionNote;
	}

	private onNewPersonNote: EventEmitter<TSocketIoNewNote & TSocketIoPerson & TGeneratorId> = new EventEmitter<TSocketIoNewNote & TSocketIoPerson & TGeneratorId>(true);
	public get newPersonNoteObservable(): Observable<TSocketIoNewNote & TSocketIoPerson & TGeneratorId> {
		return this.onNewPersonNote;
	}

	private onUpdateInteractionNote: EventEmitter<TSocketIoChangeNote & TSocketIoInteraction & TGeneratorId> = new EventEmitter<TSocketIoChangeNote & TSocketIoInteraction & TGeneratorId>(true);
	public get updateInteractionNoteObservable(): Observable<TSocketIoChangeNote & TSocketIoInteraction & TGeneratorId> {
		return this.onUpdateInteractionNote;
	}

	private onUpdatePersonNote: EventEmitter<TSocketIoChangeNote & TSocketIoPerson & TGeneratorId> = new EventEmitter<TSocketIoChangeNote & TSocketIoPerson & TGeneratorId>(true);
	public get updatePersonNoteObservable(): Observable<TSocketIoChangeNote & TSocketIoPerson & TGeneratorId> {
		return this.onUpdatePersonNote;
	}

	private onDeleteInteractionNote: EventEmitter<TSocketIoChangeNote & TSocketIoInteraction & TGeneratorId> = new EventEmitter<TSocketIoChangeNote & TSocketIoInteraction & TGeneratorId>(true);
	public get deleteInteractionNoteObservable(): Observable<TSocketIoChangeNote & TSocketIoInteraction & TGeneratorId> {
		return this.onDeleteInteractionNote;
	}

	private onDeletePersonNote: EventEmitter<TSocketIoChangeNote & TSocketIoPerson & TGeneratorId> = new EventEmitter<TSocketIoChangeNote & TSocketIoPerson & TGeneratorId>(true);
	public get deletePersonNoteObservable(): Observable<TSocketIoChangeNote & TSocketIoPerson & TGeneratorId> {
		return this.onDeletePersonNote;
	}

	private showReceiveDebug: boolean = false;
	public set receiveDebug(status: boolean) {
		this.showReceiveDebug = status;
	}

	constructor(
			statusService: CommonsStatusService,
			configService: SocketIoConfigService
	) {
		super(
				configService.getSocketIoUrl(),
				true,
				configService.getSocketIoOptions()
		);

		super.connectObservable()
				.subscribe((): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService: connected');

					statusService.setStatus('socket.io', true);
				});
		super.disconnectObservable()
				.subscribe((): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService: disconnected');

					statusService.setStatus('socket.io', false);
				});
	}
	
	protected setupOns(): void {
		this.on(
				'interactions/new',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received interactions/new', packet);

					if (!isTSocketIoInteraction(packet)) throw new Error('Invalid new interaction packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');
					this.onNewInteraction.emit(packet);
				}
		);

		this.on(
				'interactions/delete',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received interactions/delete', packet);

					if (!isTSocketIoInteraction(packet)) throw new Error('Invalid delete interaction packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');
					this.onDeleteInteraction.emit(packet);
				}
		);

		this.on(
				'interaction-steps/new',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received interaction-steps/new', packet);

					if (!isTSocketIoInteractionStep(packet)) throw new Error('Invalid new interaction packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');
					this.onNewInteractionStep.emit(packet);
				}
		);

		this.on(
				'persons/new',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received persons/new', packet);

					if (!isTSocketIoPerson(packet)) throw new Error('Invalid new person packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');
					this.onNewPerson.emit(packet);
				}
		);

		this.on(
				'persons/delete',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received persons/delete', packet);

					if (!isTSocketIoPerson(packet)) throw new Error('Invalid delete person packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');
					this.onDeletePerson.emit(packet);
				}
		);

		this.on(
				'person-interactions/link',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received person-interactions/link', packet);

					if (!isTSocketIoPersonInteraction(packet)) throw new Error('Invalid link person-interaction packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');
					this.onLinkPersonInteraction.emit(packet);
				}
		);

		this.on(
				'person-interactions/unlink',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received person-interactions/unlink', packet);

					if (!isTSocketIoPersonInteraction(packet)) throw new Error('Invalid unlink person-interaction packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');
					this.onUnlinkPersonInteraction.emit(packet);
				}
		);

		this.on(
				'datas/new',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received datas/new', packet);

					if (!isTSocketIoNewData(packet)) throw new Error('Invalid new data packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');

					if (commonsBase62HasPropertyId(packet, 'interaction')) {
						const typecast: TSocketIoNewData & TSocketIoInteraction & TGeneratorId = packet as TSocketIoNewData & TSocketIoInteraction & TGeneratorId;
						this.onNewInteractionData.emit(typecast);
					}
					if (commonsBase62HasPropertyId(packet, 'person')) {
						const typecast: TSocketIoNewData & TSocketIoPerson & TGeneratorId = packet as TSocketIoNewData & TSocketIoPerson & TGeneratorId;
						this.onNewPersonData.emit(typecast);
					}
				}
		);

		this.on(
				'datas/update',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received datas/update', packet);

					if (!isTSocketIoChangeData(packet)) throw new Error('Invalid update data packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');

					if (commonsBase62HasPropertyId(packet, 'interaction')) {
						const typecast: TSocketIoChangeData & TSocketIoInteraction & TGeneratorId = packet as TSocketIoChangeData & TSocketIoInteraction & TGeneratorId;
						this.onUpdateInteractionData.emit(typecast);
					}
					if (commonsBase62HasPropertyId(packet, 'person')) {
						const typecast: TSocketIoChangeData & TSocketIoPerson & TGeneratorId = packet as TSocketIoChangeData & TSocketIoPerson & TGeneratorId;
						this.onUpdatePersonData.emit(typecast);
					}
				}
		);

		this.on(
				'datas/delete',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received datas/delete', packet);

					if (!isTSocketIoChangeData(packet)) throw new Error('Invalid delete data packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');

					if (commonsBase62HasPropertyId(packet, 'interaction')) {
						const typecast: TSocketIoChangeData & TSocketIoInteraction & TGeneratorId = packet as TSocketIoChangeData & TSocketIoInteraction & TGeneratorId;
						this.onDeleteInteractionData.emit(typecast);
					}
					if (commonsBase62HasPropertyId(packet, 'person')) {
						const typecast: TSocketIoChangeData & TSocketIoPerson & TGeneratorId = packet as TSocketIoChangeData & TSocketIoPerson & TGeneratorId;
						this.onDeletePersonData.emit(typecast);
					}
				}
		);

		this.on(
				'notes/new',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received notes/new', packet);

					if (!isTSocketIoNewNote(packet)) throw new Error('Invalid new note packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');

					if (commonsBase62HasPropertyId(packet, 'interaction')) {
						const typecast: TSocketIoNewNote & TSocketIoInteraction & TGeneratorId = packet as TSocketIoNewNote & TSocketIoInteraction & TGeneratorId;
						this.onNewInteractionNote.emit(typecast);
					}
					if (commonsBase62HasPropertyId(packet, 'person')) {
						const typecast: TSocketIoNewNote & TSocketIoPerson & TGeneratorId = packet as TSocketIoNewNote & TSocketIoPerson & TGeneratorId;
						this.onNewPersonNote.emit(typecast);
					}
				}
		);

		this.on(
				'notes/update',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received notes/update', packet);

					if (!isTSocketIoChangeNote(packet)) throw new Error('Invalid update note packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');

					if (commonsBase62HasPropertyId(packet, 'interaction')) {
						const typecast: TSocketIoChangeNote & TSocketIoInteraction & TGeneratorId = packet as TSocketIoChangeNote & TSocketIoInteraction & TGeneratorId;
						this.onUpdateInteractionNote.emit(typecast);
					}
					if (commonsBase62HasPropertyId(packet, 'person')) {
						const typecast: TSocketIoChangeNote & TSocketIoPerson & TGeneratorId = packet as TSocketIoChangeNote & TSocketIoPerson & TGeneratorId;
						this.onUpdatePersonNote.emit(typecast);
					}
				}
		);

		this.on(
				'notes/delete',
				(packet: unknown): void => {
					if (this.showReceiveDebug) console.log('PersonTracker SocketIoService:Received notes/delete', packet);

					if (!isTSocketIoChangeNote(packet)) throw new Error('Invalid delete note packet');
					if (!isTGeneratorId(packet)) throw new Error('Invalid TGeneratorId');

					if (commonsBase62HasPropertyId(packet, 'interaction')) {
						const typecast: TSocketIoChangeNote & TSocketIoInteraction & TGeneratorId = packet as TSocketIoChangeNote & TSocketIoInteraction & TGeneratorId;
						this.onDeleteInteractionNote.emit(typecast);
					}
					if (commonsBase62HasPropertyId(packet, 'person')) {
						const typecast: TSocketIoChangeNote & TSocketIoPerson & TGeneratorId = packet as TSocketIoChangeNote & TSocketIoPerson & TGeneratorId;
						this.onDeletePersonNote.emit(typecast);
					}
				}
		);
	}
}
