import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { NgxAngularCommonsEsAppModule } from 'ngx-angularcommons-es-app';
import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';
import { NgxHttpCommonsEsHttpModule } from 'ngx-httpcommons-es-http';
import { NgxHttpCommonsEsSocketIoModule } from 'ngx-httpcommons-es-socket-io';

import { SocketIoConfigService } from './services/socket-io-config.service';
import { SocketIoService } from './services/socket-io.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsEsCoreModule,
				NgxAngularCommonsEsAppModule,
				NgxHttpCommonsEsHttpModule,
				NgxHttpCommonsEsSocketIoModule
		],
		exports: [
		],
		declarations: [
		]
})
export class NgxPersonTrackerUiSocketIoModule {
	static forRoot(): ModuleWithProviders<NgxPersonTrackerUiSocketIoModule> {
		return {
				ngModule: NgxPersonTrackerUiSocketIoModule,
				providers: [
						SocketIoConfigService,
						SocketIoService
				]
		};
	}
}
