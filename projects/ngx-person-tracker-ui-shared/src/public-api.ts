/*
 * Public API Surface of ngx-person-tracker-ui-shared
 */

export * from './lib/services/rest.service';
export * from './lib/services/config.service';

export * from './lib/ngx-person-tracker-ui-shared.module';
