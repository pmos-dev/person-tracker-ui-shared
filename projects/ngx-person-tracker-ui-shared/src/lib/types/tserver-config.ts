import { commonsTypeHasPropertyString, commonsTypeHasPropertyNumberOrUndefined } from 'tscommons-es-core';

export type TServerConfig = {
		url: string;
		timeout?: number;
		maxReattempts?: number;
};

export function isTServerConfig(test: unknown): test is TServerConfig {
	if (!commonsTypeHasPropertyString(test, 'url')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'timeout')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'maxReattempts')) return false;

	return true;
}
