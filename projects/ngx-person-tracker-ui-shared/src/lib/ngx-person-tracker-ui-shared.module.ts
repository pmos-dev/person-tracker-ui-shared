import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { NgxAngularCommonsEsAppModule } from 'ngx-angularcommons-es-app';
import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';
import { NgxHttpCommonsEsHttpModule } from 'ngx-httpcommons-es-http';

import { ConfigService } from './services/config.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsEsCoreModule,
				NgxAngularCommonsEsAppModule,
				NgxHttpCommonsEsHttpModule
		],
		exports: [
		],
		declarations: [
		]
})
export class NgxPersonTrackerUiSharedModule {
	static forRoot(): ModuleWithProviders<NgxPersonTrackerUiSharedModule> {
		return {
				ngModule: NgxPersonTrackerUiSharedModule,
				providers: [
						ConfigService
				]
		};
	}
}
