import { commonsTypeIsNumberArray, commonsTypeIsBoolean, commonsTypeIsString } from 'tscommons-es-core';
import { CommonsHttpNotFoundError } from 'tscommons-es-http';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';
import { ECommonsAdamantineFieldType } from 'tscommons-es-models-field';

import { ICondition } from 'person-tracker-ts-assets';
import { IField } from 'person-tracker-ts-assets';
import { IFlow } from 'person-tracker-ts-assets';
import { TCondition, isTCondition, decodeICondition } from 'person-tracker-ts-assets';

export class ConditionRestClientService {
	constructor(
			private restClientService: CommonsRestClientService
	) {}
	
	public async listIdsByFlow(
			flow: IFlow,
			options?: TCommonsHttpRequestOptions
	): Promise<number[]> {
		const ids: unknown = await this.restClientService.getRest<number[]>(
				`/conditions/data/${flow.id}/ids`,
				{},
				undefined,
				options
		);
		if (!commonsTypeIsNumberArray(ids)) throw new Error('Invalid ids returned');
		
		return ids;
	}
	
	public async getById(
			flow: IFlow,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<ICondition|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<TCondition>(
					`/conditions/data/${flow.id}/ids/${id}`,
					undefined,
					undefined,
					options
			);
			if (!isTCondition(encoded)) throw new Error('Invalid condition returned');
			
			return decodeICondition(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}
	
	public async createForFlow(
			flow: IFlow,
			field: IField,
			value: string|boolean|number|undefined,
			options: TCommonsHttpRequestOptions = {}
	): Promise<ICondition> {
		if (value !== undefined) {
			switch (field.type) {
				case ECommonsAdamantineFieldType.BOOLEAN:
					if (!commonsTypeIsBoolean(value)) throw new Error('Non-boolean value specified for boolean field');
					break;
				case ECommonsAdamantineFieldType.ENUM:
					if (!commonsTypeIsString(value)) throw new Error('Non-string value specified for boolean field');
					if (!field.options!.includes(value)) throw new Error('Value does not exist in field options');
					break;
			}
		}
		
		const encoded: unknown = await this.restClientService.postRest<TCondition>(
				`/conditions/data/${flow.id}`,
				{
						value: value === undefined ? null : value
				},
				undefined,
				undefined,
				options
		);
		if (!isTCondition(encoded)) throw new Error('Invalid condition returned');
			
		return decodeICondition(encoded);
	}

	public async deleteForFlowAndId(
			flow: IFlow,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<void> {
		await this.restClientService.deleteRest<boolean>(
				`/conditions/data/${flow.id}/ids/${id}`,
				undefined,
				undefined,
				options
		);
		
		// the clean-up of orphaned flows is done server-side
	}
}
