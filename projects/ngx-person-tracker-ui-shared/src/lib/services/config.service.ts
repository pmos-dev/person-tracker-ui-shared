import { Injectable } from '@angular/core';

import { TCommonsHttpRequestOptions } from 'tscommons-es-http';

import { CommonsConfigService } from 'ngx-angularcommons-es-app';

import { TServerConfig, isTServerConfig } from '../types/tserver-config';

@Injectable({
		providedIn: 'root'
})
export class ConfigService {
	private config: TServerConfig;
	
	constructor(
			configService: CommonsConfigService
	) {
		const config: unknown = configService.getDirect('server');
		if (!isTServerConfig(config)) throw new Error('Unable to read server config');
		
		this.config = config;
	}

	public getUrl(): string {
		return this.config.url;
	}
	
	public getHttpRequestOptions(): TCommonsHttpRequestOptions|undefined {
		const options: TCommonsHttpRequestOptions = {};
		
		let hasAny: boolean = false;
		if (this.config.timeout) {
			options.timeout = this.config.timeout;
			hasAny = true;
		}
		if (this.config.timeout) {
			options.maxReattempts = this.config.maxReattempts;
			hasAny = true;
		}
		
		if (!hasAny) return undefined;
		
		return options;
	}
}
