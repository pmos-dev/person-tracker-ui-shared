import { commonsDateDateToHis, commonsDateDateToYmd, commonsDateDateToYmdHis, commonsTypeIsDate, commonsTypeIsString, commonsTypeIsTArray } from 'tscommons-es-core';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';
import { ECommonsAdamantineFieldType } from 'tscommons-es-models-field';

import {
		decodeIData as decodeIPersonTrackerData,
		IData,
		IEvent,
		IField,
		IInteraction,
		IInteractionData,
		IPerson,
		IPersonData,
		isTInteractionData,
		isTPersonData,
		TData,
		TInteractionData,
		TPersonData
} from 'person-tracker-ts-assets';

function encodeValue(field: IField, value: string|number|boolean|Date): string {
	switch (field.type) {
		case ECommonsAdamantineFieldType.DATE: {
			if (!commonsTypeIsDate(value)) throw new Error('Data type does not match field of date');
			return commonsDateDateToYmd(value, true);
		}
		case ECommonsAdamantineFieldType.TIME: {
			if (!commonsTypeIsDate(value)) throw new Error('Data type does not match field of time');
			return commonsDateDateToHis(value, true);
		}
		case ECommonsAdamantineFieldType.DATETIME: {
			if (!commonsTypeIsDate(value)) throw new Error('Data type does not match field of datetime');
			return commonsDateDateToYmdHis(value, true);
		}
		case ECommonsAdamantineFieldType.ENUM: {
			if (!commonsTypeIsString(value)) throw new Error('Data type does not match field of enum');
			if (!(field.options || []).includes(value)) throw new Error('Data type is not in enum array');
			return value;
		}
		case ECommonsAdamantineFieldType.STRING: {
			if (!commonsTypeIsString(value)) throw new Error('Data type does not match field of string');
			return value;
		}
		case ECommonsAdamantineFieldType.BOOLEAN: {
			return value ? 'true' : 'false';
		}
	}
	throw new Error('Field type is not implemented');
}

export class DataRestClientService<
		DataI extends IData = IData,
		EncodedT extends TData|TInteractionData|TPersonData = TData
> {
	constructor(
			private restClientService: CommonsRestClientService,
			private isEncodedT: (test: unknown) => test is EncodedT,
			private decodeIData: (encoded: EncodedT) => DataI = decodeIPersonTrackerData
	) {}
	
	public async listByEvent(
			event: IEvent,
			options: TCommonsHttpRequestOptions = {}
	): Promise<DataI[]> {
		const encodeds: unknown = await this.restClientService.getRest<EncodedT[]>(
				`/datas/data/${event.id}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsTArray<EncodedT>(encodeds, this.isEncodedT)) throw new Error('Invalid datas returned');

		return encodeds
				.map((encoded: EncodedT): DataI => this.decodeIData(encoded));
	}
	
	public async listByEventSinceLogno(
			event: IEvent,
			logno: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<DataI[]> {
		const encodeds: unknown = await this.restClientService.getRest<EncodedT[]>(
				`/datas/data/${event.id}/since/${logno}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsTArray<EncodedT>(encodeds, this.isEncodedT)) throw new Error('Invalid datas returned');

		return encodeds
				.map((encoded: EncodedT): DataI => this.decodeIData(encoded));
	}
	
	public async listByInteraction(
			event: IEvent,
			interaction: IInteraction,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteractionData<DataI>[]> {
		const encodeds: unknown = await this.restClientService.getRest<TInteractionData[]>(
				`/datas/data/${event.id}/interactions/${interaction.id}`,
				undefined,
				undefined,
				options
		);
		// this duel step is a fudgy way for checking for both the interaction value and it being a generally valid sub-type
		if (!commonsTypeIsTArray<TInteractionData>(encodeds, isTInteractionData)) throw new Error('Invalid datas returned');
		if (!commonsTypeIsTArray<EncodedT>(encodeds, this.isEncodedT)) throw new Error('Invalid datas returned');

		return encodeds
				.map((encoded: EncodedT): IInteractionData<DataI> => this.decodeIData(encoded) as IInteractionData<DataI>);
	}
	
	public async listByPerson(
			event: IEvent,
			person: IPerson,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPersonData<DataI>[]> {
		const encodeds: unknown = await this.restClientService.getRest<TPersonData[]>(
				`/datas/data/${event.id}/persons/${person.id}`,
				undefined,
				undefined,
				options
		);
		// this duel step is a fudgy way for checking for both the interaction value and it being a generally valid sub-type
		if (!commonsTypeIsTArray<TPersonData>(encodeds, isTPersonData)) throw new Error('Invalid datas returned');
		if (!commonsTypeIsTArray<EncodedT>(encodeds, this.isEncodedT)) throw new Error('Invalid datas returned');

		return encodeds
				.map((encoded: EncodedT): IPersonData<DataI> => this.decodeIData(encoded) as IPersonData<DataI>);
	}

	public async createForInteractionAndField(
			event: IEvent,
			interaction: IInteraction,
			field: IField,
			value: string|number|boolean|Date,
			timestamp: Date,
			additionalValues: { [ key: string ]: unknown } = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteractionData<DataI>> {
		const data: unknown = await this.restClientService.postRest<
				TInteractionData,
				{
						value: string;
						timestamp: string;
				}
		>(
				`/datas/data/${event.id}/interactions/${interaction.id}/fields/${field.id}`,
				{
						value: encodeValue(field, value),
						timestamp: commonsDateDateToYmdHis(timestamp, true),
						...additionalValues
				},
				undefined,
				undefined,
				options
		);
		if (!isTInteractionData(data)) throw new Error('Invalid data created');
		if (!this.isEncodedT(data)) throw new Error('Invalid data created');

		return this.decodeIData(data) as IInteractionData<DataI>;
	}

	public async createForPersonAndField(
			event: IEvent,
			person: IPerson,
			field: IField,
			value: string|number|boolean|Date,
			timestamp: Date,
			additionalValues: { [ key: string ]: unknown } = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPersonData<DataI>> {
		const data: unknown = await this.restClientService.postRest<
				TPersonData,
				{
						value: string;
						timestamp: string;
				}
		>(
				`/datas/data/${event.id}/persons/${person.id}/fields/${field.id}`,
				{
						value: encodeValue(field, value),
						timestamp: commonsDateDateToYmdHis(timestamp, true),
						...additionalValues
				},
				undefined,
				undefined,
				options
		);
		if (!isTPersonData(data)) throw new Error('Invalid data created');
		if (!this.isEncodedT(data)) throw new Error('Invalid data created');

		return this.decodeIData(data) as IPersonData<DataI>;
	}

	public async updateData(
			event: IEvent,
			field: IField,
			data: DataI,
			value: string|number|boolean|Date,
			additionalValues: { [ key: string ]: unknown } = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<true> {
		await this.restClientService.patchRest<
				TPersonData,
				{
						value: string;
				}
		>(
				`/datas/data/${event.id}/uids/${data.uid}`,
				{
						value: encodeValue(field, value),
						...additionalValues
				},
				undefined,
				undefined,
				options
		);

		return true;
	}
		
	public async deleteData(
			event: IEvent,
			data: DataI,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean|undefined> {
		return await this.restClientService.deleteRest<boolean>(
				`/datas/data/${event.id}/uids/${data.uid}`,
				undefined,
				undefined,
				options
		);
	}
}
