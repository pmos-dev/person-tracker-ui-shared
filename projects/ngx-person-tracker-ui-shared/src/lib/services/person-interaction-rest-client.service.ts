import { commonsTypeIsNumberArray } from 'tscommons-es-core';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';

import { IInteraction, IPerson } from 'person-tracker-ts-assets';

export class PersonInteractionRestClientService {
	constructor(
			private restClientService: CommonsRestClientService
	) {}
	
	public async listByPerson(
			person: IPerson,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number[]> {
		const ids: unknown = await this.restClientService.getRest<number[]>(
				`/person-interactions/persons/${person.id}/interactions`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsNumberArray(ids)) throw new Error('Invalid interaction IDs returned');

		return ids;
	}
	
	public async listByInteraction(
			interaction: IInteraction,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number[]> {
		const ids: unknown = await this.restClientService.getRest<number[]>(
				`/person-interactions/interactions/${interaction.id}/persons`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsNumberArray(ids)) throw new Error('Invalid person IDs returned');

		return ids;
	}

	public async link(
			person: IPerson,
			interaction: IInteraction,
			options: TCommonsHttpRequestOptions = {}
	): Promise<true> {
		return await this.restClientService.putRest<true>(
				`/person-interactions/persons/${person.id}/interactions/${interaction.id}`,
				{},
				undefined,
				undefined,
				options
		);
	}

	public async unlink(
			person: IPerson,
			interaction: IInteraction,
			options: TCommonsHttpRequestOptions = {}
	): Promise<true|undefined> {
		return await this.restClientService.deleteRest<true>(
				`/person-interactions/persons/${person.id}/interactions/${interaction.id}`,
				undefined,
				undefined,
				options
		);
	}
}
