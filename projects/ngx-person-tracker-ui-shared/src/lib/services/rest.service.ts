import { commonsArrayRemoveUndefinedsPromise, TDateRange } from 'tscommons-es-core';
import { CommonsRestClientService } from 'tscommons-es-rest';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { ECommonsMoveDirection, ICommonsM2MLink } from 'tscommons-es-models';
import { ICommonsManagedModelMetadata, ICommonsManagedM2MLinkTableModelMetadata } from 'tscommons-es-models-adamantine';
import { ECommonsStepFlowStepType } from 'tscommons-es-models-step-flow';

import {
		decodeIData as decodeIPersonTrackerData,
		decodeINote as decodeIPersonTrackerNote,
		ICondition,
		IData,
		IEvent,
		IField,
		IFlow,
		IInteraction,
		IInteractionData,
		IInteractionNote,
		INamespace,
		INote,
		IPerson,
		IPersonData,
		IPersonNote,
		IStep,
		ITriage,
		TData,
		TInteractionData,
		TInteractionNote,
		TNote,
		TPersonData,
		TPersonNote
} from 'person-tracker-ts-assets';

import { NamespaceRestClientService } from './namespace-rest-client.service';
import { FieldRestClientService } from './field-rest-client.service';
import { EventRestClientService } from './event-rest-client.service';
import { TriageRestClientService } from './triage-rest-client.service';
import { StepRestClientService } from './step-rest-client.service';
import { FlowRestClientService } from './flow-rest-client.service';
import { ConditionRestClientService } from './condition-rest-client.service';
import { EventTriageRestClientService } from './event-triage-rest-client.service';
import { InteractionRestClientService } from './interaction-rest-client.service';
import { PersonRestClientService } from './person-rest-client.service';
import { PersonInteractionRestClientService } from './person-interaction-rest-client.service';
import { InteractionStepRestClientService } from './interaction-step-rest-client.service';
import { DataRestClientService } from './data-rest-client.service';
import { NoteRestClientService } from './note-rest-client.service';
import { EngineerRestClientService } from './engineer-rest-client.service';

export class PersonTrackerRestService<
		DataI extends IData = IData,
		EncodedDataT extends TData|TInteractionData|TPersonData = TData,
		NoteI extends INote = INote,
		EncodedNoteT extends TNote|TInteractionNote|TPersonNote = TNote,
> {
	private namespaceRest: NamespaceRestClientService;
	private fieldRest: FieldRestClientService;
	private eventRest: EventRestClientService;
	private triageRest: TriageRestClientService;
	private stepRest: StepRestClientService;
	private flowRest: FlowRestClientService;
	private conditionRest: ConditionRestClientService;
	private eventTriageRest: EventTriageRestClientService;
	private interactionRest: InteractionRestClientService;
	private personRest: PersonRestClientService;
	private personInteractionRest: PersonInteractionRestClientService;
	private interactionStepRest: InteractionStepRestClientService;
	private dataRest: DataRestClientService<DataI, EncodedDataT>;
	private noteRest: NoteRestClientService<NoteI, EncodedNoteT>;
	private engineerRest: EngineerRestClientService;

	constructor(
			restService: CommonsRestClientService,
			isEncodedDataT: (test: unknown) => test is EncodedDataT,
			decodeIData: (encoded: EncodedDataT) => DataI = decodeIPersonTrackerData,
			isEncodedNoteT: (test: unknown) => test is EncodedNoteT,
			decodeINote: (encoded: EncodedNoteT) => NoteI = decodeIPersonTrackerNote,
			private defaultOptions?: TCommonsHttpRequestOptions
	) {
		this.namespaceRest = new NamespaceRestClientService(restService);
		this.fieldRest = new FieldRestClientService(restService);
		this.eventRest = new EventRestClientService(restService);
		this.triageRest = new TriageRestClientService(restService);
		this.stepRest = new StepRestClientService(restService);
		this.flowRest = new FlowRestClientService(restService);
		this.conditionRest = new ConditionRestClientService(restService);
		this.eventTriageRest = new EventTriageRestClientService(restService);
		this.interactionRest = new InteractionRestClientService(restService);
		this.personRest = new PersonRestClientService(restService);
		this.personInteractionRest = new PersonInteractionRestClientService(restService);
		this.interactionStepRest = new InteractionStepRestClientService(restService);
		this.dataRest = new DataRestClientService<
				DataI,
				EncodedDataT
		>(
				restService,
				isEncodedDataT,
				decodeIData
		);
		this.noteRest = new NoteRestClientService<
				NoteI,
				EncodedNoteT
		>(
				restService,
				isEncodedNoteT,
				decodeINote
		);
		this.engineerRest = new EngineerRestClientService(restService);
	}

	public async getNamespaceMetadata(options?: TCommonsHttpRequestOptions): Promise<ICommonsManagedModelMetadata> {
		return await this.namespaceRest.getMetadata(options || this.defaultOptions);
	}

	public async listNamespaces(
			options?: TCommonsHttpRequestOptions
	): Promise<INamespace[]> {
		return await this.namespaceRest.list(options || this.defaultOptions);
	}

	public async getNamespaceById(
			id: number,
			options?: TCommonsHttpRequestOptions
	): Promise<INamespace | undefined> {
		return this.namespaceRest.getById(id, options);
	}

	public async getNamespaceByName(
			name: string,
			options?: TCommonsHttpRequestOptions
	): Promise<INamespace | undefined> {
		return this.namespaceRest.getByName(name, options);
	}

	public async getEventMetadata(options?: TCommonsHttpRequestOptions): Promise<ICommonsManagedModelMetadata> {
		return await this.eventRest.getMetadata(options || this.defaultOptions) as ICommonsManagedModelMetadata;
	}

	public async listEvents(
			namespace: INamespace,
			dateRange?: TDateRange,
			options?: TCommonsHttpRequestOptions
	): Promise<IEvent[]> {
		if (!dateRange) {
			const now: Date = new Date();
			dateRange = {
					from: now,
					to: now
			};
		}

		return await this.eventRest.listEventsByDateRange(
				namespace,
				dateRange,
				options || this.defaultOptions
		);
	}

	public async getEventById(
			namespace: INamespace,
			id: number,
			options?: TCommonsHttpRequestOptions
	): Promise<IEvent | undefined> {
		return this.eventRest.getByFirstClassAndId(
				namespace,
				id,
				options || this.defaultOptions
		);
	}

	public async getEventByUid(
			uid: string,
			options?: TCommonsHttpRequestOptions
	): Promise<IEvent | undefined> {
		return await this.eventRest.getByUid(
				uid,
				options || this.defaultOptions
		);
	}

	public async updateEvent(
			namespace: INamespace,
			event: IEvent,
			options?: TCommonsHttpRequestOptions
	): Promise<IEvent> {
		const updated: IEvent = await this.eventRest.updateForFirstClassAndId(
				namespace,
				event.id,
				event,
				options || this.defaultOptions
		);

		return updated;
	}

	public async createEvent(
			namespace: INamespace,
			event: Omit<IEvent, 'namespace' | 'id'>,
			options?: TCommonsHttpRequestOptions
	): Promise<IEvent> {
		const created: IEvent = await this.eventRest.createForFirstClass(
				namespace,
				event,
				options || this.defaultOptions
		);

		return created;
	}

	public async deleteEvent(
			namespace: INamespace,
			event: IEvent,
			options?: TCommonsHttpRequestOptions
	): Promise<void> {
		await this.eventRest.deleteForFirstClassAndId(
				namespace,
				event.id,
				options || this.defaultOptions
		);
	}

	public async getFieldMetadata(options?: TCommonsHttpRequestOptions): Promise<ICommonsManagedModelMetadata> {
		return await this.fieldRest.getMetadata(options || this.defaultOptions);
	}

	public async listFields(
			namespace: INamespace,
			options?: TCommonsHttpRequestOptions
	): Promise<IField[]> {
		return await this.fieldRest.listByFirstClass(namespace, options || this.defaultOptions);
	}

	public async getFieldById(
			namespace: INamespace,
			id: number,
			options?: TCommonsHttpRequestOptions
	): Promise<IField | undefined> {
		return this.fieldRest.getByFirstClassAndId(
				namespace,
				id,
				options || this.defaultOptions
		);
	}

	public async updateField(
			namespace: INamespace,
			field: IField,
			options?: TCommonsHttpRequestOptions
	): Promise<IField> {
		const updated: IField = await this.fieldRest.updateForFirstClassAndId(
				namespace,
				field.id,
				field,
				options || this.defaultOptions
		);

		return updated;
	}

	public async createField(
			namespace: INamespace,
			field: Omit<IField, 'namespace' | 'id'>,
			options?: TCommonsHttpRequestOptions
	): Promise<IField> {
		const created: IField = await this.fieldRest.createForFirstClass(
				namespace,
				field,
				options || this.defaultOptions
		);

		return created;
	}

	public async deleteField(
			namespace: INamespace,
			field: IField,
			options?: TCommonsHttpRequestOptions
	): Promise<void> {
		await this.fieldRest.deleteForFirstClassAndId(
				namespace,
				field.id,
				options || this.defaultOptions
		);
	}

	public async moveField(
			namespace: INamespace,
			field: IField,
			direction: ECommonsMoveDirection,
			options?: TCommonsHttpRequestOptions
	): Promise<IField> {
		return await this.fieldRest.moveForFirstClassAndIdAndDirection(
				namespace,
				field.id,
				direction,
				options
		);
	}

	public async getEventTriageMetadata(options?: TCommonsHttpRequestOptions): Promise<ICommonsManagedM2MLinkTableModelMetadata> {
		return await this.eventTriageRest.getMetadata(options || this.defaultOptions);
	}

	public async listEventTriages(
			namespace: INamespace,
			event: IEvent,
			options?: TCommonsHttpRequestOptions
	): Promise<ITriage[]> {
		const ids: number[] = await this.eventTriageRest.listBsByA(
				event,
				options || this.defaultOptions
		);

		const promises: Promise<ITriage | undefined>[] = ids
				.map((id: number): Promise<ITriage | undefined> => this.triageRest.getByFirstClassAndId(
						namespace,
						id,
						options
				));

		return commonsArrayRemoveUndefinedsPromise<ITriage>(promises);
	}

	public async linkEventTriage(
			event: IEvent,
			triage: ITriage,
			options?: TCommonsHttpRequestOptions
	): Promise<ICommonsM2MLink<IEvent, ITriage>> {
		return await this.eventTriageRest.link(
				event,
				triage,
				options || this.defaultOptions
		);
	}

	public async unlinkEventTriage(
			event: IEvent,
			triage: ITriage,
			options?: TCommonsHttpRequestOptions
	): Promise<boolean> {
		return await this.eventTriageRest.unlink(
				event,
				triage,
				false,
				options || this.defaultOptions
		);
	}

	public async getTriageMetadata(options?: TCommonsHttpRequestOptions): Promise<ICommonsManagedModelMetadata> {
		return await this.triageRest.getMetadata(options || this.defaultOptions);
	}

	public async listTriages(
			namespace: INamespace,
			options?: TCommonsHttpRequestOptions
	): Promise<ITriage[]> {
		return await this.triageRest.listByFirstClass(namespace, options || this.defaultOptions);
	}

	public async getTriageById(
			namespace: INamespace,
			id: number,
			options?: TCommonsHttpRequestOptions
	): Promise<ITriage | undefined> {
		return this.triageRest.getByFirstClassAndId(
				namespace,
				id,
				options || this.defaultOptions
		);
	}

	public async createTriage(
			namespace: INamespace,
			triage: Omit<ITriage, 'namespace' | 'id'>,
			options?: TCommonsHttpRequestOptions
	): Promise<ITriage> {
		const created: ITriage = await this.triageRest.createForFirstClass(
				namespace,
				triage,
				options || this.defaultOptions
		);

		return created;
	}

	public async updateTriage(
			namespace: INamespace,
			triage: ITriage,
			options?: TCommonsHttpRequestOptions
	): Promise<ITriage> {
		const updated: ITriage = await this.triageRest.updateForFirstClassAndId(
				namespace,
				triage.id,
				triage,
				options || this.defaultOptions
		);

		return updated;
	}

	public async deleteTriage(
			namespace: INamespace,
			triage: ITriage,
			options?: TCommonsHttpRequestOptions
	): Promise<void> {
		await this.triageRest.deleteForFirstClassAndId(
				namespace,
				triage.id,
				options || this.defaultOptions
		);
	}

	public async getTriageRootStep(
			triage: ITriage,
			options?: TCommonsHttpRequestOptions
	): Promise<IStep | undefined> {
		return this.stepRest.getRootStep(
				triage,
				options || this.defaultOptions
		);
	}

	public async getStepById(
			triage: ITriage,
			id: number,
			options?: TCommonsHttpRequestOptions
	): Promise<IStep | undefined> {
		return this.stepRest.getById(
				triage,
				id,
				options || this.defaultOptions
		);
	}

	public async createFieldStep(
			triage: ITriage,
			field: IField,
			options?: TCommonsHttpRequestOptions
	): Promise<IStep> {
		const created: IStep = await this.stepRest.createForTriageAndField(
				triage,
				field,
				options || this.defaultOptions
		);

		return created;
	}

	public async createTerminalStep(
			triage: ITriage,
			type: ECommonsStepFlowStepType.COMPLETE | ECommonsStepFlowStepType.ABORT,
			options?: TCommonsHttpRequestOptions
	): Promise<IStep> {
		const created: IStep = await this.stepRest.createForTriage(
				triage,
				type,
				options || this.defaultOptions
		);

		return created;
	}

	public async updateFieldStep(
			triage: ITriage,
			step: IStep,
			field: IField,
			options?: TCommonsHttpRequestOptions
	): Promise<IStep> {
		const updated: IStep = await this.stepRest.updateStepField(
				triage,
				step,
				field,
				options || this.defaultOptions
		);

		return updated;
	}

	public async listFlowsByStep(
			step: IStep,
			options?: TCommonsHttpRequestOptions
	): Promise<IFlow[]> {
		const ids: number[] = await this.flowRest.listIdsByStep(
				step,
				options || this.defaultOptions
		);

		const promises: Promise<IFlow | undefined>[] = ids
				.map((id: number): Promise<IFlow | undefined> => this.flowRest.getByStepAndId(
						step,
						id,
						options
				));

		return commonsArrayRemoveUndefinedsPromise<IFlow>(promises);
	}

	public async createFlowIntoNewStepForField(
			triage: ITriage,
			parent: IStep,
			field: IField,
			options?: TCommonsHttpRequestOptions
	): Promise<IFlow> {
		const dest: IStep = await this.stepRest.createForTriageAndField(triage, field, options);

		const flow: IFlow = await this.flowRest.createOutflowForStep(parent, dest, options);

		return flow;
	}

	public async createFlowIntoNewTerminalStep(
			triage: ITriage,
			parent: IStep,
			type: ECommonsStepFlowStepType.ABORT | ECommonsStepFlowStepType.COMPLETE,
			options?: TCommonsHttpRequestOptions
	): Promise<IFlow> {
		const dest: IStep = await this.stepRest.createForTriage(triage, type, options);

		const flow: IFlow = await this.flowRest.createOutflowForStep(parent, dest, options);

		return flow;
	}

	public async joinFlow(
			parent: IStep,
			dest: IStep,
			options?: TCommonsHttpRequestOptions
	): Promise<IFlow> {
		const flow: IFlow = await this.flowRest.createOutflowForStep(parent, dest, options);

		return flow;
	}

	public async moveFlow(
			step: IStep,
			flow: IFlow,
			direction: ECommonsMoveDirection,
			options?: TCommonsHttpRequestOptions
	): Promise<void> {
		await this.flowRest.moveForStepAndIdAndDirection(
				step,
				flow.id,
				direction,
				options || this.defaultOptions
		);
	}

	public async deleteFlow(
			step: IStep,
			flow: IFlow,
			options?: TCommonsHttpRequestOptions
	): Promise<void> {
		await this.flowRest.deleteForStepAndId(
				step,
				flow.id,
				options || this.defaultOptions
		);
	}

	public async listConditionsByFlow(
			flow: IFlow,
			options?: TCommonsHttpRequestOptions
	): Promise<ICondition[]> {
		const ids: number[] = await this.conditionRest.listIdsByFlow(
				flow,
				options || this.defaultOptions
		);

		const promises: Promise<ICondition | undefined>[] = ids
				.map((id: number): Promise<ICondition | undefined> => this.conditionRest.getById(
						flow,
						id,
						options
				));

		return commonsArrayRemoveUndefinedsPromise<ICondition>(promises);
	}

	public async createCondition(
			flow: IFlow,
			field: IField,
			value: string | boolean | number | undefined,
			options?: TCommonsHttpRequestOptions
	): Promise<ICondition> {
		const created: ICondition = await this.conditionRest.createForFlow(
				flow,
				field,
				value,
				options || this.defaultOptions
		);

		return created;
	}

	public async deleteCondition(
			flow: IFlow,
			condition: ICondition,
			options?: TCommonsHttpRequestOptions
	): Promise<void> {
		await this.conditionRest.deleteForFlowAndId(
				flow,
				condition.id,
				options || this.defaultOptions
		);
	}

	public async listInteractions(
			event: IEvent,
			options?: TCommonsHttpRequestOptions
	): Promise<IInteraction[]> {
		return await this.interactionRest.listByEvent(
				event,
				options || this.defaultOptions
		);
	}

	public async getInteractionById(
			event: IEvent,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteraction|undefined> {
		return await this.interactionRest.getById(
				event,
				id,
				options
		);
	}

	public async getInteractionByUid(
			uid: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteraction|undefined> {
		return await this.interactionRest.getByUid(
				uid,
				options
		);
	}

	public async createInteraction(
			event: IEvent,
			options?: TCommonsHttpRequestOptions
	): Promise<IInteraction> {
		const created: IInteraction = await this.interactionRest.createForEvent(
				event,
				options || this.defaultOptions
		);

		return created;
	}

	public async deleteInteraction(
			event: IEvent,
			interaction: IInteraction,
			options?: TCommonsHttpRequestOptions
	): Promise<void> {
		await this.interactionRest.deleteForEventAndId(
				event,
				interaction,
				options || this.defaultOptions
		);
	}

	public async listPersons(
			event: IEvent,
			options?: TCommonsHttpRequestOptions
	): Promise<IPerson[]> {
		return await this.personRest.listByEvent(
				event,
				options || this.defaultOptions
		);
	}

	public async getPersonById(
			event: IEvent,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPerson|undefined> {
		return await this.personRest.getById(
				event,
				id,
				options
		);
	}

	public async getPersonByUid(
			uid: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPerson|undefined> {
		return await this.personRest.getByUid(
				uid,
				options
		);
	}

	public async createPerson(
			event: IEvent,
			options?: TCommonsHttpRequestOptions
	): Promise<IPerson> {
		const created: IPerson = await this.personRest.createForEvent(
				event,
				options || this.defaultOptions
		);

		return created;
	}

	public async deletePerson(
			event: IEvent,
			person: IPerson,
			options?: TCommonsHttpRequestOptions
	): Promise<void> {
		await this.personRest.deleteForEventAndId(
				event,
				person,
				options || this.defaultOptions
		);
	}

	public async listInteractionIdsByPerson(
			person: IPerson,
			options?: TCommonsHttpRequestOptions
	): Promise<number[]> {
		return await this.personInteractionRest.listByPerson(
				person,
				options || this.defaultOptions
		);
	}

	public async listPersonIdsByInteraction(
			interaction: IInteraction,
			options?: TCommonsHttpRequestOptions
	): Promise<number[]> {
		return await this.personInteractionRest.listByInteraction(
				interaction,
				options || this.defaultOptions
		);
	}

	public async linkPersonInteraction(
			person: IPerson,
			interaction: IInteraction,
			options?: TCommonsHttpRequestOptions
	): Promise<true> {
		return await this.personInteractionRest.link(
				person,
				interaction,
				options || this.defaultOptions
		);
	}

	public async unlinkPersonInteraction(
			person: IPerson,
			interaction: IInteraction,
			options?: TCommonsHttpRequestOptions
	): Promise<true|undefined> {
		return await this.personInteractionRest.unlink(
				person,
				interaction,
				options || this.defaultOptions
		);
	}

	public async listInteractionSteps(
			interaction: IInteraction,
			options?: TCommonsHttpRequestOptions
	): Promise<{
			id: number;	// this is only needed so that caching can tell the difference between multiple traverals to the same step in the history
			step: string;
			timestamp: Date;
	}[]> {
		return await this.interactionStepRest.listByInteraction(
				interaction,
				options || this.defaultOptions
		);
	}

	public async createInteractionStep(
			interaction: IInteraction,
			step: string,
			timestamp: Date,
			options?: TCommonsHttpRequestOptions
	): Promise<true> {
		return await this.interactionStepRest.createForInteraction(
				interaction,
				step,
				timestamp,
				options || this.defaultOptions
		);
	}

	public async listDatasByEvent(
			event: IEvent,
			options?: TCommonsHttpRequestOptions
	): Promise<DataI[]> {
		return await this.dataRest.listByEvent(
				event,
				options || this.defaultOptions
		);
	}

	public async listDatasByInteraction(
			event: IEvent,
			interaction: IInteraction,
			options?: TCommonsHttpRequestOptions
	): Promise<IInteractionData<DataI>[]> {
		return await this.dataRest.listByInteraction(
				event,
				interaction,
				options || this.defaultOptions
		);
	}

	public async listDatasByPerson(
			event: IEvent,
			person: IPerson,
			options?: TCommonsHttpRequestOptions
	): Promise<IPersonData<DataI>[]> {
		return await this.dataRest.listByPerson(
				event,
				person,
				options || this.defaultOptions
		);
	}

	public async listDatasSinceLogno(
			event: IEvent,
			logno: number,
			options?: TCommonsHttpRequestOptions
	): Promise<DataI[]> {
		return await this.dataRest.listByEventSinceLogno(
				event,
				logno,
				options || this.defaultOptions
		);
	}

	public async createDataForInteractionAndField(
			event: IEvent,
			interaction: IInteraction,
			field: IField,
			value: string|number|boolean|Date,
			timestamp: Date,
			additionalValues: { [ key: string ]: unknown } = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteractionData<DataI>> {
		return await this.dataRest.createForInteractionAndField(
				event,
				interaction,
				field,
				value,
				timestamp,
				additionalValues,
				options || this.defaultOptions
		);
	}

	public async createDataForPersonAndField(
			event: IEvent,
			person: IPerson,
			field: IField,
			value: string|number|boolean|Date,
			timestamp: Date,
			additionalValues: { [ key: string ]: unknown } = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPersonData<DataI>> {
		return await this.dataRest.createForPersonAndField(
				event,
				person,
				field,
				value,
				timestamp,
				additionalValues,
				options || this.defaultOptions
		);
	}

	public async updateData(
			event: IEvent,
			field: IField,
			data: DataI,
			value: string|number|boolean|Date,
			additionalValues: { [ key: string ]: unknown } = {},
			options?: TCommonsHttpRequestOptions
	): Promise<true> {
		return await this.dataRest.updateData(
				event,
				field,
				data,
				value,
				additionalValues,
				options || this.defaultOptions
		);
	}

	public async deleteData(
			event: IEvent,
			data: DataI,
			options?: TCommonsHttpRequestOptions
	): Promise<boolean|undefined> {
		return await this.dataRest.deleteData(
				event,
				data,
				options || this.defaultOptions
		);
	}

	public async listNotesByEvent(
			event: IEvent,
			options?: TCommonsHttpRequestOptions
	): Promise<NoteI[]> {
		return await this.noteRest.listByEvent(
				event,
				options || this.defaultOptions
		);
	}

	public async listNotesByInteraction(
			event: IEvent,
			interaction: IInteraction,
			options?: TCommonsHttpRequestOptions
	): Promise<IInteractionNote<NoteI>[]> {
		return await this.noteRest.listByInteraction(
				event,
				interaction,
				options || this.defaultOptions
		);
	}

	public async listNotesByPerson(
			event: IEvent,
			person: IPerson,
			options?: TCommonsHttpRequestOptions
	): Promise<IPersonNote<NoteI>[]> {
		return await this.noteRest.listByPerson(
				event,
				person,
				options || this.defaultOptions
		);
	}

	public async listNotesSinceLogno(
			event: IEvent,
			logno: number,
			options?: TCommonsHttpRequestOptions
	): Promise<NoteI[]> {
		return await this.noteRest.listByEventSinceLogno(
				event,
				logno,
				options || this.defaultOptions
		);
	}

	public async createNoteForInteraction(
			event: IEvent,
			interaction: IInteraction,
			value: string,
			timestamp: Date,
			additionalValues: { [ key: string ]: unknown } = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteractionNote<NoteI>> {
		return await this.noteRest.createForInteraction(
				event,
				interaction,
				value,
				timestamp,
				additionalValues,
				options || this.defaultOptions
		);
	}

	public async createNoteForPerson(
			event: IEvent,
			person: IPerson,
			value: string,
			timestamp: Date,
			additionalValues: { [ key: string ]: unknown } = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPersonNote<NoteI>> {
		return await this.noteRest.createForPerson(
				event,
				person,
				value,
				timestamp,
				additionalValues,
				options || this.defaultOptions
		);
	}

	public async updateNote(
			event: IEvent,
			note: NoteI,
			value: string,
			additionalValues: { [ key: string ]: unknown } = {},
			options?: TCommonsHttpRequestOptions
	): Promise<true> {
		return await this.noteRest.updateNote(
				event,
				note,
				value,
				additionalValues,
				options || this.defaultOptions
		);
	}

	public async deleteNote(
			event: IEvent,
			note: NoteI,
			options?: TCommonsHttpRequestOptions
	): Promise<boolean|undefined> {
		return await this.noteRest.deleteNote(
				event,
				note,
				options || this.defaultOptions
		);
	}

	public async mergeInteractions(
			event: IEvent,
			interactions: IInteraction[],
			personData: boolean,
			options: TCommonsHttpRequestOptions = {}
	): Promise<string> {
		return await this.engineerRest.mergeInteractions(
				event,
				interactions,
				personData,
				options || this.defaultOptions
		);
	}
}
