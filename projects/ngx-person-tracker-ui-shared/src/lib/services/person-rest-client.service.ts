import { commonsTypeIsNumberArray, commonsTypeIsTArray } from 'tscommons-es-core';
import { CommonsHttpNotFoundError } from 'tscommons-es-http';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';

import { IEvent } from 'person-tracker-ts-assets';
import { IPerson } from 'person-tracker-ts-assets';
import { TPerson, isTPerson, decodeIPerson } from 'person-tracker-ts-assets';

export class PersonRestClientService {
	constructor(
			private restClientService: CommonsRestClientService
	) {}
	
	public async getById(
			event: IEvent,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPerson|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<TPerson>(
					`/persons/data/${event.id}/ids/${id}`,
					undefined,
					undefined,
					options
			);
			if (!isTPerson(encoded)) throw new Error('Invalid person returned');
			
			return decodeIPerson(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async getByUid(
			uid: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPerson|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<TPerson>(
					`/persons/data/uids/${uid}`,
					undefined,
					undefined,
					options
			);
			if (!isTPerson(encoded)) throw new Error('Invalid person returned');
			
			return decodeIPerson(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async listByEvent(
			event: IEvent,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPerson[]> {
		const encodeds: unknown = await this.restClientService.getRest<TPerson[]>(
				`/persons/data/${event.id}`,
				{},
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TPerson>(encodeds, isTPerson)) throw new Error('Invalid persons returned');
			
		return encodeds
				.map((encoded: TPerson): IPerson => decodeIPerson(encoded));
	}

	public async listIdsByEvent(
			event: IEvent,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number[]> {
		const ids: unknown = await this.restClientService.getRest<number[]>(
				`/persons/data/${event.id}/ids`,
				{},
				undefined,
				options
		);
		if (!commonsTypeIsNumberArray(ids)) throw new Error('Invalid person IDs returned');
			
		return ids;
	}
	
	public async createForEvent(
			event: IEvent,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPerson> {
		const encoded: unknown = await this.restClientService.postRest<TPerson>(
				`/persons/data/${event.id}`,
				{},
				undefined,
				undefined,
				options
		);
		if (!isTPerson(encoded)) throw new Error('Invalid person returned');
			
		return decodeIPerson(encoded);
	}
		
	public async deleteForEventAndId(
			event: IEvent,
			person: IPerson,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean|undefined> {
		return await this.restClientService.deleteRest<boolean>(
				`/persons/data/${event.id}/ids/${person.id}`,
				undefined,
				undefined,
				options
		);
	}
}
