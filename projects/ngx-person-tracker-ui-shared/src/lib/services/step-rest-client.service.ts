import { TEncodedObject } from 'tscommons-es-core';
import { CommonsHttpNotFoundError } from 'tscommons-es-http';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';
import { ECommonsStepFlowStepType } from 'tscommons-es-models-step-flow';

import { ITriage } from 'person-tracker-ts-assets';
import { IStep } from 'person-tracker-ts-assets';
import { IField } from 'person-tracker-ts-assets';
import { TStep, isTStep, decodeIStep } from 'person-tracker-ts-assets';

export class StepRestClientService {
	constructor(
			private restClientService: CommonsRestClientService
	) {}
	
	public async getRootStep(
			triage: ITriage,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IStep|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<TStep & TEncodedObject>(
					`/steps/data/${triage.id}/root-step`,
					undefined,
					undefined,
					options
			);
			if (!isTStep(encoded)) throw new Error('Invalid root step returned');
			
			return decodeIStep(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}
	
	public async getById(
			triage: ITriage,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IStep|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<TStep & TEncodedObject>(
					`/steps/data/${triage.id}/ids/${id}`,
					undefined,
					undefined,
					options
			);
			if (!isTStep(encoded)) throw new Error('Invalid step returned');
			
			return decodeIStep(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}
	
	public async createForTriageAndField(
			triage: ITriage,
			field: IField,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IStep> {
		const encoded: unknown = await this.restClientService.postRest<TStep & TEncodedObject>(
				`/steps/data/${triage.id}/field/${field.id}`,
				{},
				undefined,
				undefined,
				options
		);
		if (!isTStep(encoded)) throw new Error('Invalid step returned');
			
		return decodeIStep(encoded);
	}
	
	public async createForTriage(
			triage: ITriage,
			type: ECommonsStepFlowStepType.COMPLETE | ECommonsStepFlowStepType.ABORT,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IStep> {
		const encoded: unknown = await this.restClientService.postRest<TStep & TEncodedObject>(
				`/steps/data/${triage.id}/${type}`,
				{},
				undefined,
				undefined,
				options
		);
		if (!isTStep(encoded)) throw new Error('Invalid step returned');
			
		return decodeIStep(encoded);
	}
	
	public async updateStepField(
			triage: ITriage,
			step: IStep,
			field: IField,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IStep> {
		const encoded: unknown = await this.restClientService.patchRest<TStep & TEncodedObject>(
				`/steps/data/${triage.id}/ids/${step.id}/${field.id}`,
				{},
				undefined,
				undefined,
				options
		);
		if (!isTStep(encoded)) throw new Error('Invalid step returned');
			
		return decodeIStep(encoded);
	}
}
