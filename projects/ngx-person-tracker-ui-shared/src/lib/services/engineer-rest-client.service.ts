import { commonsBase62IsId, commonsTypeIsString } from 'tscommons-es-core';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';

import { IEvent, IInteraction } from 'person-tracker-ts-assets';

export class EngineerRestClientService {
	constructor(
			private restClientService: CommonsRestClientService
	) {}

	public async mergeInteractions(
			event: IEvent,
			interactions: IInteraction[],
			personData: boolean,
			options: TCommonsHttpRequestOptions = {}
	): Promise<string> {
		const uid: unknown = await this.restClientService.patchRest<
				string,
				{
						uids: string[];
						personData: boolean;
				}
		>(
				`/engineer/interaction/merge/${event.id}`,
				{
						uids: interactions
								.map((interaction: IInteraction): string => interaction.uid),
						personData: personData
				},
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsString(uid) || !commonsBase62IsId(uid)) throw new Error('Invalid merged UID');

		return uid;
	}
}
