import { CommonsRestClientService } from 'tscommons-es-rest';
import { CommonsAdamantineManagedOrderedModelRestClientService } from 'tscommons-es-rest-adamantine';

import { INamespace } from 'person-tracker-ts-assets';
import { TNamespace, isTNamespace, decodeINamespace } from 'person-tracker-ts-assets';

export class NamespaceRestClientService extends CommonsAdamantineManagedOrderedModelRestClientService<
		INamespace,
		TNamespace
> {
	constructor(
			restClientService: CommonsRestClientService
	) {
		super(
				restClientService,
				'/namespaces/',
				isTNamespace,
				decodeINamespace
		);
	}
}
