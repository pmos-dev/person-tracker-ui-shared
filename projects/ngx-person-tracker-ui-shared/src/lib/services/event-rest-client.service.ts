import { commonsTypeIsTArray } from 'tscommons-es-core';
import { commonsDateDateToYmd } from 'tscommons-es-core';
import { TDateRange } from 'tscommons-es-core';
import { CommonsRestClientService } from 'tscommons-es-rest';
import { CommonsAdamantineManagedSecondClassModelRestClientService } from 'tscommons-es-rest-adamantine';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';

import { IEvent } from 'person-tracker-ts-assets';
import { INamespace } from 'person-tracker-ts-assets';
import { TEvent, isTEvent, decodeIEvent } from 'person-tracker-ts-assets';

/* eslint-disable no-underscore-dangle */

export class EventRestClientService extends CommonsAdamantineManagedSecondClassModelRestClientService<
		IEvent,
		INamespace,
		TEvent
> {
	private _restClientService: CommonsRestClientService;
	
	constructor(
			restClientService: CommonsRestClientService
	) {
		super(
				restClientService,
				'/events/',
				isTEvent,
				decodeIEvent,
				true
		);
		
		this._restClientService = restClientService;
	}
	
	public async listEventsByDateRange(
			namespace: INamespace,
			dateRange: TDateRange,
			options?: TCommonsHttpRequestOptions
	): Promise<IEvent[]> {
		const events: unknown = await this._restClientService.getRest<TEvent[]>(
				`/events/data/${namespace.id}/date-range`,
				{
						from: commonsDateDateToYmd(dateRange.from, true),
						to: commonsDateDateToYmd(dateRange.to, true)
				},
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TEvent>(events, isTEvent)) throw new Error('Invalid events returned');
		
		return events
				.map((event: TEvent): IEvent => decodeIEvent(event));
	}
}
