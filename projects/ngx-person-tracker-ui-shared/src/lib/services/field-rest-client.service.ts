import { CommonsRestClientService } from 'tscommons-es-rest';
import { CommonsAdamantineManagedOrientatedOrderedModelRestClientService } from 'tscommons-es-rest-adamantine';

import { IField } from 'person-tracker-ts-assets';
import { INamespace } from 'person-tracker-ts-assets';
import { TField, isTField, decodeIField } from 'person-tracker-ts-assets';

export class FieldRestClientService extends CommonsAdamantineManagedOrientatedOrderedModelRestClientService<
		IField,
		INamespace,
		TField
> {
	constructor(
			restClientService: CommonsRestClientService
	) {
		super(
				restClientService,
				'/fields/',
				isTField,
				decodeIField,
				true
		);
	}
}
