import { commonsTypeIsNumberArray, commonsTypeIsTArray } from 'tscommons-es-core';
import { CommonsHttpNotFoundError } from 'tscommons-es-http';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';

import { IEvent } from 'person-tracker-ts-assets';
import { IInteraction } from 'person-tracker-ts-assets';
import { TInteraction, isTInteraction, decodeIInteraction } from 'person-tracker-ts-assets';

export class InteractionRestClientService {
	constructor(
			private restClientService: CommonsRestClientService
	) {}
	
	public async getById(
			event: IEvent,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteraction|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<TInteraction>(
					`/interactions/data/${event.id}/ids/${id}`,
					undefined,
					undefined,
					options
			);
			if (!isTInteraction(encoded)) throw new Error('Invalid interaction returned');
			
			return decodeIInteraction(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async getByUid(
			uid: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteraction|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<TInteraction>(
					`/interactions/data/uids/${uid}`,
					undefined,
					undefined,
					options
			);
			if (!isTInteraction(encoded)) throw new Error('Invalid interaction returned');
			
			return decodeIInteraction(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async listByEvent(
			event: IEvent,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteraction[]> {
		const encodeds: unknown = await this.restClientService.getRest<TInteraction[]>(
				`/interactions/data/${event.id}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsTArray<TInteraction>(encodeds, isTInteraction)) throw new Error('Invalid interactions returned');
			
		return encodeds
				.map((encoded: TInteraction): IInteraction => decodeIInteraction(encoded));
	}

	public async listIdsByEvent(
			event: IEvent,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number[]> {
		const ids: unknown = await this.restClientService.getRest<number[]>(
				`/interactions/data/${event.id}/ids`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsNumberArray(ids)) throw new Error('Invalid interaction IDs returned');
			
		return ids;
	}
	
	public async createForEvent(
			event: IEvent,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteraction> {
		const encoded: unknown = await this.restClientService.postRest<TInteraction>(
				`/interactions/data/${event.id}`,
				{},
				undefined,
				undefined,
				options
		);
		if (!isTInteraction(encoded)) throw new Error('Invalid interaction returned');
			
		return decodeIInteraction(encoded);
	}
		
	public async deleteForEventAndId(
			event: IEvent,
			interaction: IInteraction,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean|undefined> {
		return await this.restClientService.deleteRest<boolean>(
				`/interactions/data/${event.id}/ids/${interaction.id}`,
				undefined,
				undefined,
				options
		);
	}
}
