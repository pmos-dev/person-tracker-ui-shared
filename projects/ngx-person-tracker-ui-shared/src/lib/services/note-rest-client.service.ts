import { commonsDateDateToYmdHis, commonsTypeIsTArray } from 'tscommons-es-core';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';

import {
		decodeINote as decodeIPersonTrackerNote,
		INote,
		IEvent,
		IInteraction,
		IInteractionNote,
		IPerson,
		IPersonNote,
		isTInteractionNote,
		isTPersonNote,
		TNote,
		TInteractionNote,
		TPersonNote
} from 'person-tracker-ts-assets';

export class NoteRestClientService<
		NoteI extends INote = INote,
		EncodedT extends TNote|TInteractionNote|TPersonNote = TNote
> {
	constructor(
			private restClientService: CommonsRestClientService,
			private isEncodedT: (test: unknown) => test is EncodedT,
			private decodeINote: (encoded: EncodedT) => NoteI = decodeIPersonTrackerNote
	) {}
	
	public async listByEvent(
			event: IEvent,
			options: TCommonsHttpRequestOptions = {}
	): Promise<NoteI[]> {
		const encodeds: unknown = await this.restClientService.getRest<EncodedT[]>(
				`/notes/data/${event.id}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsTArray<EncodedT>(encodeds, this.isEncodedT)) throw new Error('Invalid notes returned');

		return encodeds
				.map((encoded: EncodedT): NoteI => this.decodeINote(encoded));
	}
	
	public async listByEventSinceLogno(
			event: IEvent,
			logno: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<NoteI[]> {
		const encodeds: unknown = await this.restClientService.getRest<EncodedT[]>(
				`/notes/data/${event.id}/since/${logno}`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsTArray<EncodedT>(encodeds, this.isEncodedT)) throw new Error('Invalid notes returned');

		return encodeds
				.map((encoded: EncodedT): NoteI => this.decodeINote(encoded));
	}
	
	public async listByInteraction(
			event: IEvent,
			interaction: IInteraction,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteractionNote<NoteI>[]> {
		const encodeds: unknown = await this.restClientService.getRest<TInteractionNote[]>(
				`/notes/data/${event.id}/interactions/${interaction.id}`,
				undefined,
				undefined,
				options
		);
		// this duel step is a fudgy way for checking for both the interaction value and it being a generally valid sub-type
		if (!commonsTypeIsTArray<TInteractionNote>(encodeds, isTInteractionNote)) throw new Error('Invalid notes returned');
		if (!commonsTypeIsTArray<EncodedT>(encodeds, this.isEncodedT)) throw new Error('Invalid notes returned');

		return encodeds
				.map((encoded: EncodedT): IInteractionNote<NoteI> => this.decodeINote(encoded) as IInteractionNote<NoteI>);
	}
	
	public async listByPerson(
			event: IEvent,
			person: IPerson,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPersonNote<NoteI>[]> {
		const encodeds: unknown = await this.restClientService.getRest<TPersonNote[]>(
				`/notes/data/${event.id}/persons/${person.id}`,
				undefined,
				undefined,
				options
		);
		// this duel step is a fudgy way for checking for both the interaction value and it being a generally valid sub-type
		if (!commonsTypeIsTArray<TPersonNote>(encodeds, isTPersonNote)) throw new Error('Invalid notes returned');
		if (!commonsTypeIsTArray<EncodedT>(encodeds, this.isEncodedT)) throw new Error('Invalid notes returned');

		return encodeds
				.map((encoded: EncodedT): IPersonNote<NoteI> => this.decodeINote(encoded) as IPersonNote<NoteI>);
	}

	public async createForInteraction(
			event: IEvent,
			interaction: IInteraction,
			value: string,
			timestamp: Date,
			additionalValues: { [ key: string ]: unknown } = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<IInteractionNote<NoteI>> {
		const note: unknown = await this.restClientService.postRest<
				TInteractionNote,
				{
						value: string;
						timestamp: string;
				}
		>(
				`/notes/data/${event.id}/interactions/${interaction.id}`,
				{
						value: value,
						timestamp: commonsDateDateToYmdHis(timestamp, true),
						...additionalValues
				},
				undefined,
				undefined,
				options
		);
		if (!isTInteractionNote(note)) throw new Error('Invalid note created');
		if (!this.isEncodedT(note)) throw new Error('Invalid note created');

		return this.decodeINote(note) as IInteractionNote<NoteI>;
	}

	public async createForPerson(
			event: IEvent,
			person: IPerson,
			value: string,
			timestamp: Date,
			additionalValues: { [ key: string ]: unknown } = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<IPersonNote<NoteI>> {
		const note: unknown = await this.restClientService.postRest<
				TPersonNote,
				{
						value: string;
						timestamp: string;
				}
		>(
				`/notes/data/${event.id}/persons/${person.id}`,
				{
						value: value,
						timestamp: commonsDateDateToYmdHis(timestamp, true),
						...additionalValues
				},
				undefined,
				undefined,
				options
		);
		if (!isTPersonNote(note)) throw new Error('Invalid note created');
		if (!this.isEncodedT(note)) throw new Error('Invalid note created');

		return this.decodeINote(note) as IPersonNote<NoteI>;
	}

	public async updateNote(
			event: IEvent,
			note: NoteI,
			value: string,
			additionalValues: { [ key: string ]: unknown } = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<true> {
		await this.restClientService.patchRest<
				TPersonNote,
				{
						value: string;
				}
		>(
				`/notes/data/${event.id}/uids/${note.uid}`,
				{
						value: value,
						...additionalValues
				},
				undefined,
				undefined,
				options
		);

		return true;
	}
		
	public async deleteNote(
			event: IEvent,
			note: NoteI,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean|undefined> {
		return await this.restClientService.deleteRest<boolean>(
				`/notes/data/${event.id}/uids/${note.uid}`,
				undefined,
				undefined,
				options
		);
	}
}
