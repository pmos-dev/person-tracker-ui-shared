import { commonsBase62HasPropertyId, commonsDateDateToYmdHis, commonsDateYmdHisToDate, commonsTypeHasPropertyNumber, commonsTypeHasPropertyString, commonsTypeIsObjectArray, COMMONS_REGEX_PATTERN_DATETIME_YMDHIS } from 'tscommons-es-core';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';

import { IInteraction } from 'person-tracker-ts-assets';

export class InteractionStepRestClientService {
	constructor(
			private restClientService: CommonsRestClientService
	) {}
	
	public async listByInteraction(
			interaction: IInteraction,
			options: TCommonsHttpRequestOptions = {}
	): Promise<{
			id: number;	// this is only needed so that caching can tell the difference between multiple traverals to the same step in the history
			step: string;
			timestamp: Date;
	}[]> {
		const stepAndTimestamps: unknown = await this.restClientService.getRest<{
				id: number;
				step: string;
				timestamp: string;
		}>(
				`/interaction-steps/interactions/${interaction.id}/steps`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsObjectArray(stepAndTimestamps)) throw new Error('Invalid interaction steps returned');

		for (const stepAndTimestamp of stepAndTimestamps) {
			if (!commonsTypeHasPropertyNumber(stepAndTimestamp, 'id')) throw new Error('Invalid interaction step returned: id');
			if (!commonsBase62HasPropertyId(stepAndTimestamp, 'step')) throw new Error('Invalid interaction step returned: step');
			if (!commonsTypeHasPropertyString(stepAndTimestamp, 'timestamp')) throw new Error('Invalid interaction step returned: timestamp');
			if (!COMMONS_REGEX_PATTERN_DATETIME_YMDHIS.test(stepAndTimestamp.timestamp as string)) throw new Error('Invalid interaction step returned: timestamp regex');
		}

		return (stepAndTimestamps as { id: number; step: string; timestamp: string }[])
				.map((stepAndTimestamp: {
						id: number;
						step: string;
						timestamp: string;
				}): {
						id: number;
						step: string;
						timestamp: Date;
				} => ({
						id: stepAndTimestamp.id,
						step: stepAndTimestamp.step,
						timestamp: commonsDateYmdHisToDate(stepAndTimestamp.timestamp, true)
				}));
	}

	public async createForInteraction(
			interaction: IInteraction,
			step: string,
			timestamp: Date,
			options: TCommonsHttpRequestOptions = {}
	): Promise<true> {
		return await this.restClientService.postRest<true>(
				`/interaction-steps/interactions/${interaction.id}/steps/${step}`,
				{
						timestamp: commonsDateDateToYmdHis(timestamp, true)
				},
				undefined,
				undefined,
				options
		);
	}
}
