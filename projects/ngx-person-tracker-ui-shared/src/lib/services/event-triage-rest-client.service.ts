import { TEncodedObject } from 'tscommons-es-core';
import { ICommonsM2MLink } from 'tscommons-es-models';
import { CommonsRestClientService } from 'tscommons-es-rest';
import { CommonsAdamantineManagedM2MModelRestClientService } from 'tscommons-es-rest-adamantine';

import { IEvent } from 'person-tracker-ts-assets';
import { ITriage } from 'person-tracker-ts-assets';

export class EventTriageRestClientService extends CommonsAdamantineManagedM2MModelRestClientService<
		IEvent,
		ITriage,
		ICommonsM2MLink<IEvent, ITriage>,
		TEncodedObject
> {
	constructor(
			restClientService: CommonsRestClientService
	) {
		super(
				restClientService,
				'/event-triages/',
				'event',
				'triage'
		);
	}
}
