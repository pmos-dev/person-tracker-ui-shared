import { CommonsRestClientService } from 'tscommons-es-rest';
import { CommonsAdamantineManagedSecondClassModelRestClientService } from 'tscommons-es-rest-adamantine';

import { ITriage } from 'person-tracker-ts-assets';
import { INamespace } from 'person-tracker-ts-assets';
import { TTriage, isTTriage, decodeITriage } from 'person-tracker-ts-assets';

export class TriageRestClientService extends CommonsAdamantineManagedSecondClassModelRestClientService<
		ITriage,
		INamespace,
		TTriage
> {
	constructor(
			restClientService: CommonsRestClientService
	) {
		super(
				restClientService,
				'/triages/',
				isTTriage,
				decodeITriage,
				true
		);
	}
}
