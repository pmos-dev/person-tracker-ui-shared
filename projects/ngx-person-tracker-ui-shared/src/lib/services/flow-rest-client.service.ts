import { commonsTypeIsNumberArray } from 'tscommons-es-core';
import { CommonsHttpNotFoundError } from 'tscommons-es-http';
import { TCommonsHttpRequestOptions } from 'tscommons-es-http';
import { CommonsRestClientService } from 'tscommons-es-rest';
import { ECommonsMoveDirection } from 'tscommons-es-models';

import { IStep } from 'person-tracker-ts-assets';
import { IFlow } from 'person-tracker-ts-assets';
import { TFlow, isTFlow, decodeIFlow } from 'person-tracker-ts-assets';

export class FlowRestClientService {
	constructor(
			private restClientService: CommonsRestClientService
	) {}
	
	public async listIdsByStep(
			step: IStep,
			options: TCommonsHttpRequestOptions = {}
	): Promise<number[]> {
		const ids: unknown = await this.restClientService.getRest<number[]>(
				`/flows/data/${step.id}/ids`,
				undefined,
				undefined,
				options
		);
		if (!commonsTypeIsNumberArray(ids)) throw new Error('Invalid ids returned');
		
		return ids;
	}
	
	public async getByStepAndId(
			step: IStep,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IFlow|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<TFlow>(
					`/flows/data/${step.id}/ids/${id}`,
					undefined,
					undefined,
					options
			);
			if (!isTFlow(encoded)) throw new Error('Invalid root flow returned');
			
			return decodeIFlow(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}
	
	public async createOutflowForStep(
			step: IStep,
			dest: IStep,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IFlow> {
		const encoded: unknown = await this.restClientService.putRest<TFlow>(
				`/flows/data/${step.id}/outsteps/${dest.id}`,
				{},
				undefined,
				undefined,
				options
		);
		if (!isTFlow(encoded)) throw new Error('Invalid flow returned');
			
		return decodeIFlow(encoded);
	}

	public async moveForStepAndIdAndDirection(
			step: IStep,
			id: number,
			direction: ECommonsMoveDirection,
			options: TCommonsHttpRequestOptions = {}
	): Promise<IFlow> {
		const encoded: unknown = await this.restClientService.patchRest<
				TFlow,
				{
						direction: ECommonsMoveDirection;
				}
		>(
				`/flows/data/${step.id}/ids/${id}/move`,
				{
						direction: direction
				},
				undefined,
				undefined,
				options
		);
		if (!isTFlow(encoded)) throw new Error('Update failure: invalid model item returned');
		
		return decodeIFlow(encoded);
	}

	public async deleteForStepAndId(
			step: IStep,
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<void> {
		await this.restClientService.deleteRest<boolean>(
				`/flows/data/${step.id}/ids/${id}`,
				undefined,
				undefined,
				options
		);
		
		// the clean-up of orphaned steps is done server-side
	}
}
